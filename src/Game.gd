extends Node3D

var level_depth = 0.2

var platform_height = 2


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	# make character look like they go up and down
	var front_wall_z = $Level/ZBounds/Front.global_position.z
	var back_wall_z = $Level/ZBounds/Back.global_position.z
	

	var offset_percent = (front_wall_z - $Player.global_position.z) / (front_wall_z - back_wall_z)
	$Player.project(platform_height * offset_percent, ( 1 - level_depth) +   (1 - offset_percent) * (level_depth * 2))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$Camera.position.x = $Player.position.x
	

