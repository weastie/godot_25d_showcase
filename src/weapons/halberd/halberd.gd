extends Area3D

var attacking = false
var enemies_hit_this_attack = []

var damage = 6

func attack():
	if !attacking:
		enemies_hit_this_attack = []
		$AnimationPlayer.play("attack")
		attacking = true
		

func _on_animation_player_animation_finished(anim_name):
	if anim_name == 'attack':
		attacking = false


func _on_area_entered(area):
	if attacking:
		if !enemies_hit_this_attack.has(area.get_instance_id()):
			enemies_hit_this_attack.append(area.get_instance_id())
