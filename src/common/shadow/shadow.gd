extends Node3D

var base_position = Vector3(0, 0, 0)
var base_scale = Vector3(0, 0, 0)


func project(y_offset, scale_factor, parent_height):
	$Sprite.position.y = base_position.y + y_offset + (parent_height - scale_factor * parent_height) / 2
	$Sprite.scale = base_scale * scale_factor

# Called when the node enters the scene tree for the first time.
func _ready():
	base_position = $Sprite.position
	base_scale = $Sprite.scale

func _physics_process(delta):
	# Force shadow onto the ground
	if $ShadowCast.is_colliding():
		var collider = $ShadowCast.get_collider()
		var shape_id = $ShadowCast.get_collider_shape()
		var shape_owner_id = collider.shape_find_owner(shape_id)
		var shape_owner = collider.shape_owner_get_owner(shape_owner_id)
		global_position.y = shape_owner.global_position.y
	else:
		print('not')

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
