extends CharacterBody3D


const SPEED = 4
const JUMP_VELOCITY = 6

var base_position = Vector3(0, 0, 0)
var base_scale = Vector3(0, 0, 0)

var direction_facing = 1

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func project(y_offset, scale_factor):
	$Projection.position.y = base_position.y + y_offset
	$Projection.scale = base_scale * scale_factor
	$Shadow.project(y_offset, scale_factor, $Projection/Sprite.get_item_rect().size.y * $Projection/Sprite.pixel_size)

func flip_direction():
	$Projection/Sprite.flip_h = !$Projection/Sprite.flip_h
	$Projection/WeaponNode.scale.x *= -1
	$Projection/WeaponNode.position.x *= -1
	direction_facing *= -1

func _input(event):
	if event.is_action_pressed('attack'):
		if $Projection/WeaponNode.has_node("Weapon"):
			$Projection/WeaponNode.get_node("Weapon").attack()

func _ready():
	base_position = $Projection/Sprite.position
	base_scale = $Projection/Sprite.scale

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("move_left", "move_right", "move_back", "move_close")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)
		
	if velocity.x != 0 || velocity.z != 0:
		$Projection/Sprite.play("walk")
		if (velocity.x > 0 && direction_facing == -1) || (velocity.x < 0 && direction_facing == 1):
			flip_direction()
	else:
		$Projection/Sprite.play("default")

	move_and_slide()
	

